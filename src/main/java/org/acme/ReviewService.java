package org.acme;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;


@ApplicationScoped
public class ReviewService {

    @Inject
    ReviewRepository reviewRepository;

    @Inject
    MovieRepository movieRepository;

    public Review createReview(String reviewBody , String imdbId) {
        Review review = new Review(reviewBody);
        review.persist();

        Movie movie = movieRepository.findMovieByImdbId(imdbId);
        if (movie != null) {
            movie.getReviewIds().add(review.id);
            movie.persistOrUpdate();
        }

        return review;
    }
}


