package org.acme;
import java.util.Map;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;

import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/api/v1/reviews")
public class ReviewController {

    @Inject
    ReviewService reviewService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createReview(Map<String, String> payload) {
        Review review = reviewService.createReview(payload.get("body"), payload.get("imdbId"));
        return Response.status(Response.Status.CREATED).entity(review).build();
    }
}
