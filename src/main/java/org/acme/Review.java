package org.acme;

import io.quarkus.mongodb.panache.common.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import jakarta.persistence.Id;
import org.bson.types.ObjectId;

@MongoEntity(collection = "reviews")
@Data
@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
@NoArgsConstructor
public class Review extends PanacheMongoEntity {
    @Id
    ObjectId id; 
    String body;

    public Review(String body) {
        this.body = body;
    }
}
