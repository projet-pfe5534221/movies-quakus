package org.acme;

import org.bson.types.ObjectId;
import io.quarkus.mongodb.panache.common.MongoEntity;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.Id;
import io.quarkus.mongodb.panache.PanacheMongoEntity;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@MongoEntity(collection = "movies")
@Data
@EqualsAndHashCode(callSuper=false)
@ApplicationScoped
@AllArgsConstructor
@NoArgsConstructor
public class Movie extends PanacheMongoEntity {
    @Id
    private ObjectId id;

    
    private String imdbId;
    private String title;
    private String releaseDate;
    private String trailerLink;
    private String poster;
    private List<String> genres;
    private List<String> backdrops;

    List<ObjectId> reviewIds;

    public Movie findMovieByImdbId(String imdbId) {
        return find("imdbId", imdbId).firstResult();
    }
    
}
