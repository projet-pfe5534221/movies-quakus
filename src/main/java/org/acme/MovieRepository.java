package org.acme;


import io.quarkus.mongodb.panache.PanacheMongoRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class MovieRepository implements PanacheMongoRepository<Movie> {

    
    public Movie findMovieByImdbId(String imdbId) {
        return find("imdbId", imdbId).firstResult();
    }
    }
