package org.acme;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@Path("/api/v1/movies")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MovieResource {
    

    @Inject
    private MovieService movieService;

    @GET
    public Response getAllMovies() {
        List<Movie> movies = movieService.allMovies();
        return Response.status(Response.Status.OK).entity(movies).build();
    }

    @GET
    @Path("/{imdbId}")
    public Response getSingleMovie(@PathParam("imdbId") String imdbId) {
        Optional<Movie> movie = movieService.singleMovie(imdbId);
        if (movie.isPresent()) {
            return Response.status(Response.Status.OK).entity(movie.get()).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }}}