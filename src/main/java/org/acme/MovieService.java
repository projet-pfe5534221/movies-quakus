package org.acme;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import java.util.List;
import java.util.Optional;

@ApplicationScoped

public class MovieService {
    @Inject
    private MovieRepository movieRepository;

    public List<Movie> allMovies() {
        return movieRepository.findAll().list();
    }

    public Optional<Movie> singleMovie(String imdbId) {
        return Optional.ofNullable(movieRepository.findMovieByImdbId(imdbId));
    }

    
}
