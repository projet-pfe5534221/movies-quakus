package org.acme;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

@QuarkusTest
public class MovieIntegrationTest {

    @BeforeAll
    public static void setUp() {
        RestAssured.config = RestAssuredConfig.config().httpClient(HttpClientConfig.httpClientConfig()
            .setParam("http.connection.timeout", 10000)
            .setParam("http.socket.timeout", 10000)
            .setParam("http.connection-manager.timeout", 10000));
    }

    @Test
    public void testGetAllMovies() {
        given()
          .when().get("/api/v1/movies")
          .then()
             .statusCode(200);
    }

    @Test
    public void testGetMovieByImdbId() {
        given()
          .when().get("/api/v1/movies/tt11116912")
          .then()
             .statusCode(200);
    }
}
